/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package flightreservation.Database;
import java.sql.*;
import javax.swing.DefaultListModel;
/**
 *
 * @author taohuh
 */
public class DBConnect {
    Connection connection = null;
    String conString = "http://reportsolution.co/phpmyadmin/";
    String username = "cs285-2558_g17";
    String password = "DxXDBqPk";
    
    //RETRIEVE DATA
    public DefaultListModel retrieve(){
        DefaultListModel dm = new DefaultListModel();
        
        //SQL STMT
        String sql = "SELECT * FROM flightDetail";
        
        try{
            connection = DriverManager.getConnection(conString,username,password);
            
            //PREPARED STMT
            Statement statement = connection.prepareStatement(sql);
            ResultSet rs = statement.executeQuery(sql);
            
            while(rs.next()){
                String airportName = rs.getString("airportName");
                double price = rs.getDouble("price");
                dm.addElement(airportName);
                dm.addElement(price);
            }
            return dm;        
        }catch(Exception ex){
            ex.printStackTrace();
        }
        return null;
    }
    
}    
