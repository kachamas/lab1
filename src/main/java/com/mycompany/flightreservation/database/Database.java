/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package flightreservation.Database;
import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author taohuh
 */
public class Database {
    public static void main(String[] args) throws SQLException{
        try {
            Class.forName("com.mysql.jdbc.Driver");
            try (Connection connect = DriverManager.getConnection("jdbc:mysql://localhost:3306/flight", "root", null)) {
                PreparedStatement ps;
                ps = connect.prepareStatement("SELECT * FROM flightDetail");
                ResultSet result = ps.executeQuery();
                while(result.next()){
                    System.out.println("ชื่อสายการบิน :" + result.getString("airportName"));
                    System.out.println("ราคา: " + result.getDouble("price"));
                }
                ps.close();
            }
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Database.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
