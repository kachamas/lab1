/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package flightreservation.view;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.*;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import flightreservation.controller.MyFlightDetail;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableModel;
/**
 *
 * @author taohuh
 */
public class MyForm extends JFrame {
    
    public String airlineTo;
    public String airlineFrom;
    
    CardLayout cards;
    JPanel cardPanel;
    public static void main(String[] arg){
        EventQueue.invokeLater(new Runnable(){
            @Override
            public void run() {
                MyForm form = new MyForm();
                form.setVisible(true);
            }
            
        });
    }
    
    public MyForm(){
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(800,450);
        setLocation(500, 280);
        
        cards = new CardLayout();
        cardPanel = new JPanel();
        cardPanel.setLayout(cards);
        
        //Panel1
        JPanel panel1 = new JPanel();
        getContentPane().add(panel1,BorderLayout.NORTH);
        //panel1.setLayout(null);

        //Label Title
        JLabel lblTitle = new JLabel("FLIGHT");
        lblTitle.setBounds(320, 19, 250, 55);
        lblTitle.setFont(new java.awt.Font("Tahoma", 1, 36));
        lblTitle.setHorizontalTextPosition(SwingConstants.CENTER);
        lblTitle.setVerticalTextPosition(SwingConstants.BOTTOM);
        panel1.add(lblTitle);
        
        /*
        // Label FlyingFrom
        JLabel lblFlyingFrom = new JLabel("From :");
        lblFlyingFrom.setBounds(40, 69, 99, 55);
        lblFlyingFrom.setFont(new java.awt.Font("Tahoma", 1, 21));
        panel1.add(lblFlyingFrom);
        
        // Label FlyingTo
        JLabel lblFlyingTo = new JLabel("To :");
        lblFlyingTo.setBounds(40, 119, 99, 55);
        lblFlyingTo.setFont(new java.awt.Font("Tahoma", 1, 21));
        panel1.add(lblFlyingTo);
        
        // Input Flying From
        final JTextField flyingFrom = new JTextField();
        flyingFrom.setBounds(125,83, 129, 25);
        panel1.add(flyingFrom);
        
        // Input FlyingTo
        final JTextField flyingTo = new JTextField();
        flyingTo.setBounds(125,133, 129, 25);
        panel1.add(flyingTo);
        */
        
        //Button Search
        JButton searchBtn = new JButton("Search");
        searchBtn.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                cards.next(cardPanel);
            }
        });
        searchBtn.setBounds(170, 370, 80, 30);
        searchBtn.setHorizontalTextPosition(SwingConstants.CENTER);
        searchBtn.setVerticalTextPosition(SwingConstants.BOTTOM);
        panel1.add(searchBtn);
        
        
        JPanel panel2 = new JPanel();
        //ScrollPane
        JScrollPane scrollPane = new JScrollPane();
        scrollPane.setBounds(14,14,700,89);
        panel2.add(scrollPane);
        
        //Table
        JTable table = new JTable();
        scrollPane.setViewportView(table);
        //panel2.add(scrollPane);
        //Model for Table
        DefaultTableModel model = (DefaultTableModel)table.getModel();
        model.addColumn("Price");
        model.addColumn("Airline");
        model.addColumn("Take-off");
        model.addColumn("Landing");
        
        Connection connect = null;
        //Statement s = null;
        
        try {
            Class.forName("com.mysql.jdbc.Driver");
            connect = DriverManager.getConnection("jdbc:mysql://localhost:3306/flight", "root", null);
            //s = connect.createStatement();
           
            PreparedStatement ps;
            ps = connect.prepareStatement("SELECT * FROM flightDetail");
            ResultSet result = ps.executeQuery();
            
            int row = 0;
            while((result!=null) && (result.next())){
                model.addRow(new Object[0]);
                model.setValueAt(result.getDouble("price"),row,0);
                model.setValueAt(result.getString("airportName"), row, 1);
                model.setValueAt(result.getString("Take-off"), row, 2);
                model.setValueAt(result.getString("Landing"), row, 3);
                row++;
            }
            result.close();
            ps.close();
        }catch (Exception e) {
            //Logger.getLogger(Database.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, e.getMessage());
            e.printStackTrace();
        }
        
        cardPanel.add(panel1, "panel1");
        cardPanel.add(panel2, "panel2");
        
        //getContentPane().add(mainPanel,BorderLayout.NORTH);
        getContentPane().add(cardPanel,BorderLayout.CENTER);
    }
}
